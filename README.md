# Santun FEM Builder

A Julia library for generating finite element meshes from inputs such as
magnetic resonance imaging data.
