@testset "NodeCloud construction" begin

	@testset "Successful CartesianNodeCloud{T} construction" begin

		vec = [1,2,3,4,5,6,7,8,9]

		mat = [1 2 3 ; 4 5 6 ; 7 8 9]

		node = CartesianNode{Int64}(1,2,3)

		vecnodecloud = CartesianNodeCloud{Int64}(vec,nothing)

		matnodecloud = CartesianNodeCloud{Int64}(mat,nothing)

		node2cloud = CartesianNodeCloud{Int64}(node,nothing)

		vcn1 = vecnodecloud[1]
		vcn2 = vecnodecloud[2]
		vcn3 = vecnodecloud[3]

		mcn1 = matnodecloud[1]
		mcn2 = matnodecloud[2]
		mcn3 = matnodecloud[3]

		n2cn = node2cloud[1]

		@test vcn1.x == 1 && vcn1.y == 2 && vcn1.z == 3
		@test vcn2.x == 4 && vcn2.y == 5 && vcn2.z == 6
		@test vcn3.x == 7 && vcn3.y == 8 && vcn3.z == 9

		@test mcn1.x == 1 && mcn1.y == 4 && mcn1.z == 7
		@test mcn2.x == 2 && mcn2.y == 5 && mcn2.z == 8
		@test mcn3.x == 3 && mcn3.y == 6 && mcn3.z == 9

		@test n2cn.x == 1 && n2cn.y == 2 && n2cn.z == 3

	end

end

@testset "CartesianNodeCloud{T} indexing" begin

	mat = [ 1 2 3 ; 4 5 6 ; 7 8 9 ; 10 11 12 ; 13 14 15 ; 16 17 18 ; 19 20 21 ; 22 23 24 ]

	nc = CartesianNodeCloud{Float64}(mat,nothing)

	@testset "Indexing with single Shape{N}s" begin

		s1 = Point(1)
		s2 = Edge(1,3)
		s3 = Triangle(1,3,5)
		s4 = Tetrahedron(1,3,5,6)
		s5 = Shape{5}(1,6,2,3,5)
		s6 = Hexahedron(1,6,2,3,5,4,7,8)

		sc1 = nc[s1]
		sc2 = nc[s2]
		sc3 = nc[s3]
		sc4 = nc[s4]
		sc5 = nc[s5]
		sc6 = nc[s6]

		sc1n1 = sc1[1]

		@test sc1n1.x == 1 && sc1n1.y == 2 && sc1n1.z == 3

		sc2n1 = sc2[1]
		sc2n2 = sc2[2]

		@test sc2n1.x == 1 && sc2n1.y == 2 && sc2n1.z == 3
		@test sc2n2.x == 7 && sc2n2.y == 8 && sc2n2.z == 9

		sc3n1 = sc3[1]
		sc3n2 = sc3[2]
		sc3n3 = sc3[3]

		@test sc3n1.x == 1  && sc3n1.y == 2  && sc3n1.z == 3
		@test sc3n2.x == 7  && sc3n2.y == 8  && sc3n2.z == 9
		@test sc3n3.x == 13 && sc3n3.y == 14 && sc3n3.z == 15

		sc4n1 = sc4[1]
		sc4n2 = sc4[2]
		sc4n3 = sc4[3]
		sc4n4 = sc4[4]

		@test sc4n1.x == 1  && sc4n1.y == 2  && sc4n1.z == 3
		@test sc4n2.x == 7  && sc4n2.y == 8  && sc4n2.z == 9
		@test sc4n3.x == 13 && sc4n3.y == 14 && sc4n3.z == 15
		@test sc4n4.x == 16 && sc4n4.y == 17 && sc4n4.z == 18

		sc5n1 = sc5[1]
		sc5n2 = sc5[2]
		sc5n3 = sc5[3]
		sc5n4 = sc5[4]
		sc5n5 = sc5[5]

		@test sc5n1.x == 1  && sc5n1.y == 2  && sc5n1.z == 3
		@test sc5n2.x == 16 && sc5n2.y == 17 && sc5n2.z == 18
		@test sc5n3.x == 4  && sc5n3.y == 5  && sc5n3.z == 6
		@test sc5n4.x == 7  && sc5n4.y == 8  && sc5n4.z == 9
		@test sc5n5.x == 13 && sc5n5.y == 14 && sc5n5.z == 15

		sc6n1 = sc6[1]
		sc6n2 = sc6[2]
		sc6n3 = sc6[3]
		sc6n4 = sc6[4]
		sc6n5 = sc6[5]
		sc6n6 = sc6[6]
		sc6n7 = sc6[7]
		sc6n8 = sc6[8]

		@test sc6n1.x == 1  && sc6n1.y == 2  && sc6n1.z == 3
		@test sc6n2.x == 16 && sc6n2.y == 17 && sc6n2.z == 18
		@test sc6n3.x == 4  && sc6n3.y == 5  && sc6n3.z == 6
		@test sc6n4.x == 7  && sc6n4.y == 8  && sc6n4.z == 9
		@test sc6n5.x == 13 && sc6n5.y == 14 && sc6n5.z == 15
		@test sc6n6.x == 10 && sc6n6.y == 11 && sc6n6.z == 12
		@test sc6n7.x == 19 && sc6n7.y == 20 && sc6n7.z == 21
		@test sc6n8.x == 22 && sc6n8.y == 23 && sc6n8.z == 24

	end

	@testset "Indexing with multiple Shape{N}s" begin

		s1 = Triangle(1,2,3)
		s2 = Edge(4,5)

		nodes = nc[s1,s2]

		n1 = nodes[1]
		n2 = nodes[2]
		n3 = nodes[3]
		n4 = nodes[4]
		n5 = nodes[5]

		@test n1.x == 1  && n1.y == 2  && n1.z == 3
		@test n2.x == 4  && n2.y == 5  && n2.z == 6
		@test n3.x == 7  && n3.y == 8  && n3.z == 9
		@test n4.x == 10 && n4.y == 11 && n4.z == 12
		@test n5.x == 13 && n5.y == 14 && n5.z == 15


	end
end
