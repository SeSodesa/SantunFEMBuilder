@testset "CartesianNode construction" begin

	@testset "Inner constructors" begin

		cnode = CartesianNode{Float32,AnyNodeData{String}}(
			1.0,
			2.0,
			3.0,
			AnyNodeData{String}("data")
		)

		@test begin
			cnode.x === Float32(1) &&
			cnode.y === Float32(2) &&
			cnode.z === Float32(3) &&
			cnode.data.data == "data"
		end

		a1node = CartesianNode{
			Rational{Int8},
			AnyNodeData{Int16}
		}(
			[1],
			AnyNodeData{Int16}(7)
		)

		@test begin
			a1node.x === Rational{Int8}(1) &&
			a1node.y === Rational{Int8}(1) &&
			a1node.z === Rational{Int8}(1) &&
			a1node.data.data == Int16(7)
		end

		a3node = CartesianNode{
			Rational{Int8},
			AnyNodeData{Int16}
		}(
			[1,3,5],
			AnyNodeData{Int16}(15)
		)

		@test begin
			a3node.x === Rational{Int8}(1) &&
			a3node.y === Rational{Int8}(3) &&
			a3node.z === Rational{Int8}(5) &&
			a3node.data.data == Int16(15)
		end

	end

	@testset "Outer constructors" begin

		d = AnyNodeData(1)

		n1 = CartesianNode(1)

		@test begin
			n1.x === 1 &&
			n1.y === 1 &&
			n1.z === 1 &&
			n1.data === nothing
		end

		n2 = CartesianNode(1,d)

		@test begin
			n2.x === 1 &&
			n2.y === 1 &&
			n2.z === 1 &&
			n2.data.data === 1
		end

		n3 = CartesianNode(1,2,3)

		@test begin
			n3.x === 1 &&
			n3.y === 2 &&
			n3.z === 3 &&
			n3.data === nothing
		end

		n4 = CartesianNode(1,2,3, d)

		@test begin
			n4.x === 1 &&
			n4.y === 2 &&
			n4.z === 3 &&
			n4.data.data === 1
		end

		n5 = CartesianNode([1])

		@test begin
			n5.x === 1 &&
			n5.y === 1 &&
			n5.z === 1 &&
			n5.data === nothing
		end

		n6 = CartesianNode([1,2,3])

		@test begin
			n6.x === 1 &&
			n6.y === 2 &&
			n6.z === 3 &&
			n6.data === nothing
		end

		n7 = CartesianNode([1],d)

		@test begin
			n7.x === 1 &&
			n7.y === 1 &&
			n7.z === 1 &&
			n7.data.data === 1
		end

		n8 = CartesianNode([1,2,3],d)

		@test begin
			n8.x === 1 &&
			n8.y === 2 &&
			n8.z === 3 &&
			n8.data.data === 1
		end

		n9 = CartesianNode{Rational{Int32}}(1)

		@test begin
			n9.x === Rational{Int32}(1) &&
			n9.y === Rational{Int32}(1) &&
			n9.z === Rational{Int32}(1) &&
			n9.data === nothing
		end

		n10 = CartesianNode{Rational{Int32}}(1,2,3)

		@test begin
			n10.x === Rational{Int32}(1) &&
			n10.y === Rational{Int32}(2) &&
			n10.z === Rational{Int32}(3) &&
			n10.data === nothing
		end

		n11 = CartesianNode{Rational{Int32}}(1,2,3,d)

		@test begin
			n11.x === Rational{Int32}(1) &&
			n11.y === Rational{Int32}(2) &&
			n11.z === Rational{Int32}(3) &&
			n11.data.data === 1
		end

		n12 = CartesianNode{Rational{Int32}}([1,2,3],d)

		@test begin
			n12.x === Rational{Int32}(1) &&
			n12.y === Rational{Int32}(2) &&
			n12.z === Rational{Int32}(3) &&
			n12.data.data === 1
		end

	end

	@testset "Succesful initialization of CartesianNode / 3 with no data" begin

		i64node = CartesianNode{Int64}(1.0, 2.0, 3.0)
		i32node = CartesianNode{Int32}(1.0, 2.0, 3.0)
		r64node = CartesianNode{Rational{Int64}}(1.0, 2.0, 3.0)
		r32node = CartesianNode{Rational{Int32}}(1.0, 2.0, 3.0)
		f64node = CartesianNode{Float64}(1.0, 2.0, 3.0)
		f32node = CartesianNode{Float32}(1.0, 2.0, 3.0)

		@test i64node.x === Int64(1) && i64node.y === Int64(2) && i64node.z === Int64(3)

		@test i32node.x === Int32(1) && i32node.y === Int32(2) && i32node.z === Int32(3)

		@test begin
			r64node.x === Rational{Int64}(1) &&
			r64node.y === Rational{Int64}(2) &&
			r64node.z === Rational{Int64}(3)
		end

		@test begin
			r32node.x === Rational{Int32}(1) &&
			r32node.y === Rational{Int32}(2) &&
			r32node.z === Rational{Int32}(3)
		end

		@test f64node.x === Float64(1) && i64node.y === Int64(2) && i64node.z === Int64(3)

		@test f32node.x === Float32(1) && i32node.y === Int32(2) && i32node.z === Int32(3)

		@test isnothing(i64node.data)
		@test isnothing(r32node.data)
		@test isnothing(f32node.data)
		@test isnothing(i64node.data)
		@test isnothing(r64node.data)
		@test isnothing(f64node.data)

	end

	@testset "Succesful initialization of CartesianNode / 1" begin

		i64node = CartesianNode{Int64}(2.0)
		i32node = CartesianNode{Int32}(2.0)
		r64node = CartesianNode{Rational{Int64}}(2.0)
		r32node = CartesianNode{Rational{Int32}}(2.0)
		f64node = CartesianNode{Float64}(2.0)
		f32node = CartesianNode{Float32}(2.0)

		@test i64node.x == Int64(2) &&i64node.y == Int64(2) && i64node.z == Int64(2)

		@test i32node.x == Int32(2) &&i32node.y == Int32(2) && i32node.z == Int32(2)

		@test r64node.x == 2 // 1 && r64node.y == 2 // 1 && i64node.z == 2 // 1

		@test r32node.x == 2 // 1 && r32node.y == 2 // 1 && i32node.z == 2 // 1

		@test f64node.x == Float64(2) &&i64node.y == Float64(2) && i64node.z == Float64(2)

		@test f32node.x == Float32(2) &&i32node.y == Float32(2) && i32node.z == Float32(2)

		@test isnothing(i64node.data)
		@test isnothing(r32node.data)
		@test isnothing(f32node.data)

		@test isnothing(i64node.data)
		@test isnothing(r64node.data)
		@test isnothing(f64node.data)

	end

	@testset "CartesianNodes with data" begin

		ld = LabelData("some label")
		adi = AnyNodeData(1)
		ads = AnyNodeData("string")

		a1node = CartesianNode([1],ld)
		a3node = CartesianNode([1,2,3],ld)

		inode = CartesianNode{Float64}(1,2,3, adi)
		snode = CartesianNode{Float64}(1,2,3, ads)

		@test a1node.data.label == "some label"
		@test a3node.data.label == "some label"

		@test inode.data.data == 1
		@test snode.data.data == "string"

	end

	@testset "Unsuccesful initialization of CartesianNode / 3" begin

		@test_throws DomainError CartesianNode{Float64}(1.0, 2, NaN)

		@test_throws DomainError CartesianNode{Float64}(1.0, 2, Inf)

		@test_throws ArgumentError CartesianNode([1, 2])

	end

	@testset "Unsuccesful initialization of CartesianNode / 1" begin

		@test_throws DomainError CartesianNode{Float64}(NaN)

		@test_throws DomainError CartesianNode{Float64}(Inf)

	end

end
