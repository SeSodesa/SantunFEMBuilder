@testset "Shape{N} construction" begin

	@testset "Succesful initialization of Shape{N}" begin

		point = Point(8)

		edge = Edge(1,3)

		triangle = Triangle(1,3,5)

		tetrahedron = Tetrahedron(4,1,3,2)

		polyhedron5 = Shape{5}(1,2,3,4,5)

		hexahedron = Hexahedron(1,2,3,4,5,6,7,8)

		polyhedron10 = Shape{10}(1,4,6,7,3,2,5,9,10,8)

		vec = [ 1, 2, 3, 4, 5, 6, 7, 8 ]

		mat = [ 1 2 ; 3 4 ; 5 6 ; 7 8 ]

		hexa2 = Hexahedron(vec)

		hexa3 = Hexahedron(mat)

		@test point.inds == (8,)

		@test edge.inds == (1,3)

		@test triangle.inds == (1,3,5)

		@test tetrahedron.inds == (4,1,3,2)

		@test polyhedron5.inds == (1,2,3,4,5)

		@test hexahedron.inds == (1,2,3,4,5,6,7,8)

		@test polyhedron10.inds == (1,4,6,7,3,2,5,9,10,8)

		@test hexa2.inds == (1,2,3,4,5,6,7,8)

		@test hexa3.inds == (1,3,5,7,2,4,6,8)

	end

	@testset "Unsuccesful initialization of Shape{N}" begin

		vec = [1,2,3,2,5]

		rotations = Vector{Vector{Int}}(undef,5)

		for ii ∈ 1:5

			rotations[ii] = vec

			vec = circshift(vec,1)

		end

		@test_throws DimensionMismatch point = Point(8,3)

		@test_throws DimensionMismatch edge = Edge(1)

		@test_throws DimensionMismatch triangle = Triangle(1,3)

		@test_throws DimensionMismatch tetrahedron = Tetrahedron(1,2,3,3)

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(1,2,3,4)

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(rotations[1])

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(rotations[2])

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(rotations[3])

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(rotations[4])

		@test_throws DimensionMismatch polyhedron5 = Shape{5}(rotations[5])

		@test_throws DimensionMismatch hexahedron = Hexahedron(1,2,3,4,5,6,7)

	end

end
