module Nodes

export CartesianNode

import ..Option

using ..AbstractTypes
using ..NodeData

"""
	CartesianNode

A node which contains a set of Cartesian coordinates as numbers <: Real,
and a node data instance.
"""
struct CartesianNode{T,D} <: AbstractNode where {
	T <: Real,
	D <: Option{<:AbstractNodeData}
}

	x :: T
	y :: T
	z :: T

	data :: D

	"""
		CartesianNode{T,D}(::T1, ::T2, ::T3)

	Constructs a CartesianNode object with given x-, y- and z-coordinates.
	Nodes with NaN or Inf coordinates do not make sense, so these are
	disallowed and throw and exception in order to fail fast.
	"""
	function CartesianNode{T,D}(x::T1, y::T2, z::T3, d::AD) where {
		T  <: Real,
		T1 <: Real,
		T2 <: Real,
		T3 <: Real,
		D  <: Option{<:AbstractNodeData},
		AD <: Option{<:AbstractNodeData},
	}

		has_nan_vals = any(isnan.((x,y,z)))
		has_inf_vals = any(isinf.((x,y,z)))

		if has_nan_vals
			throw(DomainError(NaN, "Cannot construct CartesianNodes with NaN coordinates."))
		end

		if has_inf_vals
			throw(DomainError(Inf, "Cannot construct CartesianNodes with ∞ coordinates."))
		end

		new{T,D}(x, y, z, d)

	end # function

	"""
		CartesianNode{T}(::AbstractArray{A})

	Constructs a CartesianNode object with an array of length 1 or 3.
	"""
	function CartesianNode{T,D}(arr::AbstractArray{A}, d::AD) where {
		T  <: Real,
		D  <: Option{<:AbstractNodeData},
		A  <: Real,
		AD <: Option{<:AbstractNodeData},
	}

		if length(arr) == 1

			CartesianNode{T,D}(arr[1], arr[1], arr[1], d)

		elseif length(arr) == 3

			CartesianNode{T,D}(arr[1], arr[2], arr[3], d)

		else

			throw(ArgumentError("CartesianNodes can only be initialized with arrays of length 1 or 3."))

		end

	end # function

end # CartesianNode

## Outer constructors for convenience
#
# These are here to allow node construction without passing in all of the
# needed type parameters and/or arguments needed by the inner constructors.
# Start with 1-argument versions.

function CartesianNode(c::T) where { T }

	CartesianNode{T,Nothing}(c,c,c,nothing)

end

function CartesianNode(c::T, d::D) where { T, D }

	CartesianNode{T,D}(c,c,c,d)

end

function CartesianNode{T1}(c::T2) where { T1, T2 }

	CartesianNode{T1,Nothing}(c,c,c,nothing)

end

function CartesianNode{T1}(c::T2, d::D) where { T1, T2, D }

	CartesianNode{T1,D}(c, c, c, d)

end

# Three-argument versions.

function CartesianNode(x::T, y::T, z::T) where { T }

	CartesianNode{T,Nothing}(x,y,z,nothing)

end

function CartesianNode(x::T, y::T, z::T, d::D) where { T, D }

	CartesianNode{T,D}(x,y,z,d)

end

function CartesianNode{T}(x::T1, y::T2, z::T3) where { T, T1, T2, T3 }

	CartesianNode{T,Nothing}(x, y, z, nothing)

end

function CartesianNode{T}(x::T1, y::T2, z::T3, d::D) where { T, T1, T2, T3, D }

	CartesianNode{T,D}(x,y,z,d)

end

# Array versions.

function CartesianNode(arr::AbstractArray{A}) where { A }

	CartesianNode{A,Nothing}(arr, nothing)

end


function CartesianNode(arr::AbstractArray{A}, d::D) where { A, D }

	CartesianNode{A,D}(arr, d)

end

function CartesianNode{T}(arr::AbstractArray{A}) where { T, A }

	CartesianNode{T,Nothing}(arr, nothing)

end

function CartesianNode{T}(arr::AbstractArray{A}, d::D) where { T, A, D }

	CartesianNode{T,D}(arr, d)

end

## External functions
#
# Define how nodes are displayed, among other things.

function Base.show(io::IO, n::CartesianNode{T, M}) where { T, M }
	print(io, "CartesianNode(x: $(n.x), y: $(n.y), z: $(n.z), data: $(n.data))")
end

function Base.show(io::IO, ::MIME"text/plain", n::CartesianNode{T,M}) where { T, M }
	summary(io, n)
	println()
	println(io, "   x: $(n.x)")
	println(io, "   y: $(n.y)")
	println(io, "   z: $(n.z)")
	println(io, "data: $(n.data)")
end

function Base.:+(node::CartesianNode)
	node
end

function Base.:+(a::CartesianNode, b::CartesianNode)
	CartesianNode(a.x + b.x, a.y + b.y, a.z + b.z)
end

function Base.:-(n::CartesianNode)
	CartesianNode(-n.x, -n.y, -n.z)
end

function Base.:-(a::CartesianNode, b::CartesianNode)
	a + -b
end

function Base.:*(a::CartesianNode, b::CartesianNode)
	CartesianNode(a.x * b.x, a.y * b.y, a.z * b.z)
end

function Base.:(==)(a::CartesianNode, b::CartesianNode)
	a.x == b.x && a.y == b.y && a.z == b.z
end

function Base.length(::CartesianNode)
	3
end

function Base.size(::CartesianNode)
	(3,)
end

function Base.getindex(cn::CartesianNode, I::Int)

	if I == 1
		cn.x
	elseif I == 2
		cn.y
	elseif I == 3
		cn.z
	else
		throw(BoundsError(I, "CartesianNodes have exactly 3 indices"))
	end

end # function

function Base.getindex(cn::CartesianNode{T}, I::Int...) where { T <: Number }

	n_of_vals = length(I)

	vals = Vector{T}(undef, n_of_vals)

	for ind ∈ 1 : n_of_vals

		if I[ind] == 1
			vals[ind] = cn.x
		elseif I[ind] == 2
			vals[ind] = cn.y
		elseif I[ind] == 3
			vals[ind] = cn.z
		else
			throw(BoundsError(ind, "CartesianNodes have exactly 3 indices"))
		end

	end

	vals

end # function

function Base.eltype(cn::CartesianNode{A,B}) where { A, B }
	A
end

function Base.convert(::Type{Vector}, cn::CartesianNode{A,B}) where {A, B}
	[cn.x, cn.y, cn.z]
end

end # module
