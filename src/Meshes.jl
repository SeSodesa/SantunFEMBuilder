module Meshes

	using ..AbstractTypes
	using ..Nodes
	using ..NodeClouds
	using ..Shapes

	"""
		ShapeMesh{C,N}

	Represents and ordered pair ⟨NC,SS⟩, where NC is a node cloud and SS the
	set of shapes or tuples of indices, which define which nodes form which
	shape.
	"""
	struct ShapeMesh{CT, NT} <: AbstractMesh where {
		CT <: AbstractNode,
		NT <: Integer
	}

		nodes :: NodeCloud{CT}
		shapes :: Vector{Shape{NT}}

		"""
			Mesh(::NodeCloud{N})
		"""
		function ShapeMesh(nc::NodeCloud{RT}, sv::Vector{Shape{IT}}) where { RT, IT }

			validate_shapes_fn(nc)

			error("TODO")

		end

	end

	"""
		validate_shapes_fn(::NodeCLoud, ::Vector{<:AbstractShape})

	Attempts to index into the nodes with each shape in shapes. If a shape
	references an out of bounds node, returns false, else returns true.
	"""
	function validate_shapes_fn(nodes::NodeCloud, shapes::AbstractArray{<:Shape})

		ℓ = length(nodes)

		for (i, shape) ∈ enumerate(shapes)

			for ind ∈ shape.inds

				if ind < 1 || ind > ℓ
					throw(DomainError(
						ind,
						"$(shape) number $i contains an index $ind outside of allowed node indices."
					))
				end

			end # for

		end # for

	end # function

end # module
