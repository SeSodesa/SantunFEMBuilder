module NodeClouds

	export NodeCloud, CartesianNodeCloud

	export cartesian_cloud_fn, spherical_cloud_fn

	import ..Option

	using ..AbstractTypes
	using ..Shapes
	using ..Nodes
	using ..NodeData
	using ..NodeCloudData

	"""
		NodeCloud{A,B}

	A subtype of AbstractNodeCloud, parameterized over subtypes T and metadata
	M related to groups of these.
	"""
	struct NodeCloud{A,B} <: AbstractNodeCloud where {
		A <: AbstractNode,
		B <: AbstractNodeCloudData
	}

		nodes :: Vector{A}
		meta :: B

		"""
			NodeCloud{A,B}(abstract_node, optional_metadata)

		Creates a node cloud from a single node.
		"""
		function NodeCloud{A,B}(node::C, meta::D) where {
			A <: AbstractNode,
			B <: AbstractNodeCloudData,
			C <: AbstractNode,
			D <: AbstractNodeCloudData,
		}

			new([node], meta)

		end

		"""
			NodeCloud{A,B}(nodes, metadata)

		Constructs a node cloud from a given abstract array by straightening
		it into a vector.
		"""
		function NodeCloud{A,B}(nodes::AbstractVector{C}, meta::D) where {
			A <: AbstractNode,
			B <: AbstractNodeCloudData,
			C <: AbstractNode,
			D <: AbstractNodeCloudData,
		}

			new(nodes, meta)

		end

		"""
			NodeCloud{A,B}(nodes, metadata)

		Constructs a node cloud from a given abstract array by straightening
		it into a vector.
		"""
		function NodeCloud{A,B}(nodes::AbstractArray{C}, meta::D) where {
			A <: AbstractNode,
			B <: AbstractNodeCloudData,
			C <: AbstractNode,
			D <: AbstractNodeCloudData,
		}

			new(nodes[:], meta)

		end

	end # NodeCloud

	function Base.size(nc::NodeCloud)
		size(nc.nodes)
	end

	function Base.length(nc::NodeCloud)
		length(nc.nodes)
	end

	function Base.getindex(nc::NodeCloud, I::Int)
		getindex(nc.nodes, I)
	end

	function Base.getindex(nc::NodeCloud, I::Int...)
		getindex(nc.nodes, I...)
	end

	function Base.getindex(nc::NodeCloud, I::UnitRange{Int})
		getindex(nc.nodes, I)
	end

	function Base.getindex(nc::NodeCloud, I::Colon)
		getindex(nc.nodes, I)
	end

	function Base.getindex(nc::NodeCloud, I::Shape)
		[nc.nodes[ind] for ind ∈ I.inds]
	end

	function Base.getindex(nc::NodeCloud, I::Shape...)
		[nc.nodes[ind] for t ∈ I for ind ∈ t.inds]
	end

	function Base.setindex!(nc::NodeCloud, v::T, I::Int) where { T <: AbstractNode }
		setindex!(nc.nodes, v, I)
	end

	function Base.setindex!(nc::NodeCloud, v::T, I::Int...) where { T <: AbstractNode }
		setindex!(nc.nodes, v, I...)
	end

	function Base.firstindex(nc::NodeCloud)
		firstindex(nc.nodes)
	end

	function Base.lastindex(nc::NodeCloud)
		lastindex(nc.nodes)
	end

	function Base.iterate(nc::T) where { T <: NodeCloud }
		iterate(nc.nodes)
	end

	function Base.iterate(nc::T, I::Int) where { T <: NodeCloud }
		iterate(nc.nodes, I)
	end

	function Base.show(io::IO, x::NodeCloud{A,B}) where { A, B }
		println("nodes: ", x.nodes)
		println("meta: ", x.meta)
	end

	function Base.show(io::IO, m::MIME"text/plain", x::NodeCloud{A,B}) where { A, B }

		print(io, "Cloud type: "); summary(io, x); println(io)

		print(io, "Cloud data: "); show(io, m, x.meta)

		println(io)

		for node ∈ x
			println(io)
			show(io, m, node)
		end

	end

	## CartesianNodeCloud{T}
	#
	# A type definition for a Cartesian node cloud.

	"""
		CartesianNodeCloud{A,B}

	A type alias for NodeCloud{CartesianNode{A,B}, CartesianNodeCloudData}.
	"""
	const CartesianNodeCloud{A} = NodeCloud{CartesianNode{A,B}, CartesianNodeCloudData} where { B }

	"""
		CartesianNodeCloud{T}(::N)

	Constructs a Cartesian node cloud from a single node.
	"""
	function CartesianNodeCloud{A}(node::B) where {
		A,
		B <: CartesianNode,
	}

		nodes = [node]

		meta = CartesianNodeCloudData(nodes)

		CartesianNodeCloud{A}(nodes, meta)

	end

	"""
		CartesianNodeCloud{T}(::AbstractVector{N})

	Constructs a Cartesian node cloud from a vector of numbers. Works
	differently if the vector length is not divisible by 3.
	"""
	function CartesianNodeCloud{A}(
		numbers::AbstractVector{B},
		nodedata::C
	) where {
		A,
		B,
		C <: Option{<:AbstractArray{<:AbstractNodeData}}
	}

		ℓ = length(numbers)

		ndl = isnothing(nodedata) ? nothing : length(nodedata)

		divisible_by_3 = ℓ % 3 == 0

		nodes = if divisible_by_3

			veclen = div(ℓ, 3)

			if ! isnothing(nodedata) @assert(ndl == veclen) end

			vec = Vector{CartesianNode}(undef, veclen)

			for (ni, vi) ∈ zip(1 : 3 : ℓ, 1 : veclen)

				nd = isnothing(nodedata) ? nothing : nodedata[vi]

				x = numbers[ni]
				y = numbers[ni+1]
				z = numbers[ni+2]

				vec[vi] = CartesianNode{A}(x,y,z,nd)

			end

			vec

		else # assume indentical xyz-coordinates

			vec = Vector{CartesianNode}(undef, ℓ)

			if ! isnothing(nodedata) @assert(ndl == ℓ) end

			for ind ∈ 1 : ℓ

				nd = isnothing(nodedata) ? nothing : nodedata[ind]

				c = numbers[ind]

				vec[ind] = CartesianNode{A}(c,nd)

			end

			vec

		end

		meta = CartesianNodeCloudData(nodes)

		NodeCloud{CartesianNode{A}, CartesianNodeCloudData}(nodes, meta)

	end # function

	"""
		CartesianNodeCloud{A}(numbers::AbstractMatrix{B})

	Constructs a Cartesian node cloud from a matrix of numbers.
	"""
	function CartesianNodeCloud{A}(
		node::B,
		nodedata::C
	) where {
		A,
		B <: CartesianNode,
		C <: Option{<:AbstractNodeData}
	}

		nodes = node

		meta = CartesianNodeCloudData([node])

		NodeCloud{CartesianNode{A}, CartesianNodeCloudData}(node,meta)

	end

	"""
		CartesianNodeCloud{A}(numbers::AbstractMatrix{B})

	Constructs a Cartesian node cloud from a matrix of numbers.
	"""
	function CartesianNodeCloud{A}(
		numbers::AbstractMatrix{B},
		nodedata::C
	) where {
		A,
		B,
		C <: Option{<:AbstractArray{<:AbstractNodeData}}
	}

		n_of_rows = size(numbers, 1)
		n_of_cols = size(numbers, 2)

		dℓ = isnothing(nodedata) ? nothing : length(nodedata)

		has_3_rows = n_of_rows == 3
		has_3_cols = n_of_cols == 3

		# Prioritize matrices with 3 rows over 3 columns for processor cache
		# efficiency. This branching seems to be necessary for whatever
		# reason. If the input matrix has neither 3 rows nor columns, dispatch
		# on the vector method of this function.

		nodes, iterdim =
		if has_3_rows && has_3_cols

			! isnothing(nodedata) && @assert(dℓ == n_of_rows)

			Vector{CartesianNode{A}}(undef, n_of_cols), "cols"

		elseif has_3_rows && ! has_3_cols

			! isnothing(nodedata) && @assert(dℓ == n_of_cols)

			Vector{CartesianNode{A}}(undef, n_of_rows), "cols"

		elseif ! has_3_rows && has_3_cols

			! isnothing(nodedata) && @assert(dℓ == n_of_rows)

			Vector{CartesianNode{A}}(undef, n_of_rows), "rows"

		else

			return CartesianNodeCloud{A}(numbers[:], nodedata)

		end

		n_of_nodes = length(nodes)

		for ind ∈ 1 : n_of_nodes

			nd = isnothing(nodedata) ? nothing : nodedata[ind]

			if iterdim == "rows"

				x = numbers[ind, 1]
				y = numbers[ind, 2]
				z = numbers[ind, 3]

			elseif iterdim == "cols"

				x = numbers[1,ind]
				y = numbers[2,ind]
				z = numbers[3,ind]

			else

				error("Could not iterate over columns nor rows. The heck?")

			end

			nodes[ind] = CartesianNode{A}(x,y,z, nd)

		end

		meta = CartesianNodeCloudData(nodes)

		NodeCloud{CartesianNode{A},CartesianNodeCloudData}(nodes, meta)

	end # function

	function Base.convert(::Type{Matrix}, nc::NodeCloud{CartesianNode{A}, B}) where { A, B }
		[
			[n.x for n ∈ nc] [n.y for n ∈ nc] [n.z for n ∈ nc]
		]
	end

	function Base.convert(::Type{Vector}, nc::NodeCloud{A,B}) where { A, B }
		nc.nodes
	end

	"""
		cartesian_cloud_fn(data, type)

	A helper function for generating Cartesian node clouds of given type.
	"""
	function cartesian_cloud_fn(data, type::Type{T}) where { T <: Real }
		NodeCloud{CartesianNode{type}}(data)
	end

	"""
		shperical_cloud_fn(center, radius, resolution, element_type)

	A helper function for generating a spherical point cloud of a given radius
	R around a given center C, with a given angular (NOT surface) resolution
	in radians. Note that at larger values of R, the surface resolution will
	be diminished, even with small values of resolution.
	"""
	function spherical_cloud_fn(
		cp::A,
		r::B,
		resolution::C,
		element_type::D,
		label::E
	) where {
		A <: NTuple{3, <:Real},
		B <: Real,
		C <: Real,
		D <: Type{<:Real},
		E <: AbstractChar
	}

		θs = inclinations = 0 : resolution : π

		φs = azimuths = 0 : resolution : 2π

		θ_φ_pairs = Iterators.product(θs,φs)

		xs = ( r * cos(φ) * sin(θ) + cp[1] for (θ, φ) ∈ θ_φ_pairs )

		ys = ( r * sin(φ) * sin(θ) + cp[2] for (θ, φ) ∈ θ_φ_pairs )

		zs = ( r * cos(θ) + cp[3] for (θ, _) ∈ θ_φ_pairs )

		xyz = [ collect(xs)[:] collect(ys)[:] collect(zs)[:] ]

		uxyz = unique(xyz, dims=1)

		labels = [ AnyNodeData(label) for _ in 1 : size(uxyz,1) ]

		CartesianNodeCloud{element_type}(uxyz, labels)

	end

	function spherical_cloud_fn(
		Cs::A,
		Rs::B,
		resolutions::C,
		ets::D,
		labels::E,
	) where {
		A <: Vector{<:NTuple{3, <:Real}},
		B <: Vector{<:Real},
		C <: Vector{<:Real},
		D <: Vector{DataType},
		E <: Vector{<:AbstractChar},
	}

		n_of_centers = length(Cs)

		n_of_radii = length(Rs)

		n_of_resolutions = length(resolutions)

		n_of_element_types = length(ets)

		n_of_labels = length(ets)

		@assert(n_of_centers == n_of_radii == n_of_resolutions == n_of_element_types == n_of_labels)

		clouds = Dict{Char, Any}()

		sizehint!(clouds, n_of_labels)

		for ind ∈ 1 : n_of_centers

			key = labels[ind]

			clouds[key] = spherical_cloud_fn(Cs[ind], Rs[ind], resolutions[ind], ets[ind], labels[ind])

		end

		clouds

	end

end # module
