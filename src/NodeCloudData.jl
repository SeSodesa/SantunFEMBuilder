module NodeCloudData

	export CartesianNodeCloudData

	using ..AbstractTypes
	using ..Nodes

	"""
		CartesianNodeCloudData

	A type for holding additional information about CartesianNodes, such as
	the indices of the nodes with smallest and largest xyz-coordinates.
	"""
	struct CartesianNodeCloudData <: AbstractNodeCloudData

		# Indices of nodes with smallest x-, y- and z-coordinates.

		xmin :: Int
		ymin :: Int
		zmin :: Int

		# Indices of nodes with largest x-, y- and z-coordinates.

		xmax :: Int
		ymax :: Int
		zmax :: Int

		function CartesianNodeCloudData(nodes::AbstractArray{T}) where { T <: CartesianNode }

			xmin = 0
			ymin = 0
			zmin = 0

			xmax = 0
			ymax = 0
			zmax = 0

			for (i,n) ∈ enumerate(nodes)

				if i == 1

					xmin = xmax = i
					ymin = ymax = i
					zmin = zmax = i

				else

					if n.x < nodes[xmin].x
						xmin = i
					end

					if n.x > nodes[xmax].x
						xmax = i
					end

					if n.y < nodes[ymin].y
						ymin = i
					end

					if n.y > nodes[ymax].y
						ymax = i
					end

					if n.z < nodes[zmin].z
						zmin = i
					end

					if n.z > nodes[zmax].z
						zmax = i
					end

				end

			end # for

			new(xmin,ymin,zmin,xmax,ymax,zmax)

		end # function

	end # CartesianNodeCloudData

end # module
