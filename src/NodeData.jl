module NodeData

	export AnyNodeData, LabelData

	using ..AbstractTypes

	"""
		AnyNodeData

	A node data container that can hold a single piece of data of any type.
	"""
	struct AnyNodeData{T} <: AbstractNodeData where { T <: Any }
		data :: T
	end

	"""
		LabelData

	A data type for holding a textual label.
	"""
	struct LabelData{T} <: AbstractNodeData where { T <: AbstractString }
		label :: T
	end

end # module
