"""
	module AbstractTypes

A module that holds abstract supertypes of the library.
"""
module AbstractTypes

	export AbstractNode
	export AbstractNodeData
	export AbstractNodeCloudData
	export AbstractNodeCloud
	export AbstractShape
	export AbstractMesh

	"""
		AbstractNode

	An abstract finite element mesh node superclass, which actual nodes will
	derive from.
	"""
	abstract type AbstractNode end

	"""
		AbstractNodeCloudData

	An abstract supertype of additional metadata related to different node
	types. Subtypes are stored in node clouds, as necessary.
	"""
	abstract type AbstractNodeCloudData end

	"""
		AbstractNodeCloud

	An abstract supertype of node clouds.
	"""
	abstract type AbstractNodeCloud end

	"""
		AbstractNodeData

	An abstract supertype of datatypes that contain node data.
	"""
	abstract type AbstractNodeData end

	"""
		AbstractShape

	An abstract shape type, which other shapes derive from.
	"""
	abstract type AbstractShape end

	"""
		AbstractMesh

	An abstract supertype of different kinds of meshes.
	"""
	abstract type AbstractMesh end

end # module
