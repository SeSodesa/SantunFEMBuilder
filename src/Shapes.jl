module Shapes

	export Shape, Point, Edge, Triangle, Tetrahedron, Hexahedron

	using ..AbstractTypes

	"""
		Shape{N}

	Shapes are used to index into point clouds, to produce the coordinates of the
	shapes' nodes. A shape type parameterized over the number of indices it holds.
	For example, specifying a Shape{3}(...) defines a shape that holds 3 node
	indices, which one might interpret as a triangle.
	"""
	struct Shape{N} <: AbstractShape where { N }

		inds :: NTuple{N, Int}

		function Shape{N}(inds::NTuple{N,Int}) where { N }

			validate_indices_fn(inds, Shape{N})

			new{N}(inds)

		end

		function Shape{N}(inds::Vararg{Int}) where { N }

			validate_indices_fn(inds, Shape{N})

			new{N}(inds)

		end

		function Shape{N}(inds::AbstractArray) where { N }

			validate_indices_fn(inds, Shape{N})

			inds = (inds...,)

			new{N}(inds)

		end

	end # Shape

	function Base.show(io::IO, s::Shape{N}) where { N }

		if N == 1
			print(io, Point, s.inds)
		elseif N == 2
			print(io, Edge, s.inds)
		elseif N == 3
			print(io, Triangle, s.inds)
		elseif N == 4
			print(io, Tetrahedron, s.inds)
		elseif N == 8
			print(io, Hexahedron, s.inds)
		else
			print(io, "Polyhedron", s.inds)
		end

	end

	function Base.show(io::IO, ::MIME"text/plain", s::Shape{N}) where { N }

		if N == 1
			print(io, Point, s.inds)
		elseif N == 2
			print(io, Edge, s.inds)
		elseif N == 3
			print(io, Triangle, s.inds)
		elseif N == 4
			print(io, Tetrahedron, s.inds)
		elseif N == 8
			print(io, Hexahedron, s.inds)
		else
			print(io, "Polyhedron", s.inds)
		end

	end

	function Base.getindex(s::Shape{N}, I::Int) where { N }
		s.inds[I]
	end

	function Base.getindex(s::Shape{N}, I::Int...) where { N }
		([s.inds[i] for i ∈ I]...,)
	end


	"""
		Point

	A type alias for Shape{1}.
	"""
	const Point = Shape{1}

	"""
		Edge

	A type alias for Shape{2}.
	"""
	const Edge = Shape{2}


	"""
		Triangle

	A type alias for Shape{3}.
	"""
	const Triangle = Shape{3}

	"""
		Tetrahedron

	A type alias for Shape{4}.
	"""
	const Tetrahedron = Shape{4}

	"""
		Hexahedron

	A type alias for Shape{8}.
	"""
	const Hexahedron = Shape{8}

	"""
		validate_indices_fn

	Checks that a given set of inputs forms a valid set of shape indices: they
	should be positive and separate from each other.
	"""
	function validate_indices_fn(node_inds, shape_type::Type{Shape{N}}) where { N }

		# Check that we have a correct number of elements.

		ℓ = length(node_inds)

		if ℓ != N
			throw(DimensionMismatch("$(shape_type) constructor got $ℓ arguments but expected $N."))
		end

		# First check for positivity.

		if ! all(node_inds .> 0)
			throw(DomainError(node_inds, "Node indices in a $(shape_type) must be positive whole numbers."))
		end

		# Then check for uniqueness. Generate a hash set for checking element
		# existence.

		index_set = Set{Int}()

		# Define a value for determining index existence in the above index set.

		DEFAULT = -1

		# Try inserting keys into the hash set. If a key already exists, there is
		# a duplicate, so fail fast.

		for ind ∈ node_inds

			if ind ∈ index_set

				throw(DimensionMismatch("The shape $(shape_type) must have $N DIFFERENT node indices."))

			else

				# This causes a second hash, but that will have to do for now.

				push!(index_set, ind)

			end

		end

	end # function

end # module
